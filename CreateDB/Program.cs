﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrinocoAccess;

namespace CreateDB
{
    class Program
    {
        static void Main(string[] args)
        {
            const string DB_FILE = "users.db";
            const string DEFAULT_PW = "password";

            OrinocoDB userDB = new OrinocoDB(DB_FILE);
            OrinocoUser newUser = new OrinocoUser();
            DepartmentAccess debtAccess;
            uint id = 1000;

            if (userDB != null)
            {
                Console.WriteLine("Creating Database...");
                // Create user for each department
                // using default password
                // with full access rights
                foreach (Department dept in Enum.GetValues(typeof(Department)))
                {
                    newUser.EmployeeID = ++id;
                    newUser.Username = dept.ToString().ToLower();
                    newUser.Password = Hash.ToHash(DEFAULT_PW);
                    debtAccess = new DepartmentAccess(dept, AccessLevel.Read | AccessLevel.Write | AccessLevel.Create | AccessLevel.Delete);
                    newUser.AddDepartment(debtAccess);
                    Console.WriteLine("    Adding user " + newUser.Username);
                    userDB.Users.Add(newUser);
                    newUser = new OrinocoUser();                    
                }

                try
                {
                    Console.WriteLine("Saving Database to " +  DB_FILE);
                    userDB.Save();
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error: " + ex.Message);
                }

                Console.WriteLine("Done.");
                Console.WriteLine("Press any key to close");
                Console.ReadKey(false);                
            }

        } // Main
    }
}
