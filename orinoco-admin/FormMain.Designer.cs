﻿namespace Orinoco_Admin
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.mnuMain = new System.Windows.Forms.MenuStrip();
            this.mnuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuRunBackup = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSaveDB = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.saveDbDialog = new System.Windows.Forms.SaveFileDialog();
            this.openDbDialog = new System.Windows.Forms.OpenFileDialog();
            this.lstUsers = new System.Windows.Forms.ListBox();
            this.txtEmployeeID = new System.Windows.Forms.TextBox();
            this.lblEmployeeID = new System.Windows.Forms.Label();
            this.lblUsername = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.lstDepartmentAccess = new System.Windows.Forms.ListBox();
            this.grpAccessLevel = new System.Windows.Forms.GroupBox();
            this.optCreate = new System.Windows.Forms.CheckBox();
            this.optDelete = new System.Windows.Forms.CheckBox();
            this.optWrite = new System.Windows.Forms.CheckBox();
            this.optRead = new System.Windows.Forms.CheckBox();
            this.lblUsers = new System.Windows.Forms.Label();
            this.grpUserInfo = new System.Windows.Forms.GroupBox();
            this.btnChangePassword = new System.Windows.Forms.Button();
            this.btnSaveChanges = new System.Windows.Forms.Button();
            this.cboDepartments = new System.Windows.Forms.ComboBox();
            this.lblDepartments = new System.Windows.Forms.Label();
            this.btnAddDepartment = new System.Windows.Forms.Button();
            this.btnRemoveDepartment = new System.Windows.Forms.Button();
            this.grpDepartmentAccess = new System.Windows.Forms.GroupBox();
            this.btnNewUser = new System.Windows.Forms.Button();
            this.btnDeleteUser = new System.Windows.Forms.Button();
            this.mnuMain.SuspendLayout();
            this.grpAccessLevel.SuspendLayout();
            this.grpUserInfo.SuspendLayout();
            this.grpDepartmentAccess.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnuMain
            // 
            this.mnuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFile});
            this.mnuMain.Location = new System.Drawing.Point(0, 0);
            this.mnuMain.Name = "mnuMain";
            this.mnuMain.Size = new System.Drawing.Size(733, 24);
            this.mnuMain.TabIndex = 8;
            this.mnuMain.Text = "menuStrip1";
            // 
            // mnuFile
            // 
            this.mnuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuRunBackup,
            this.mnuSaveDB,
            this.toolStripMenuItem1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Size = new System.Drawing.Size(37, 20);
            this.mnuFile.Text = "&File";
            // 
            // mnuRunBackup
            // 
            this.mnuRunBackup.Name = "mnuRunBackup";
            this.mnuRunBackup.Size = new System.Drawing.Size(152, 22);
            this.mnuRunBackup.Text = "Run &Backup";
            this.mnuRunBackup.Click += new System.EventHandler(this.mnuRunBackup_Click);
            // 
            // mnuSaveDB
            // 
            this.mnuSaveDB.Name = "mnuSaveDB";
            this.mnuSaveDB.Size = new System.Drawing.Size(152, 22);
            this.mnuSaveDB.Text = "&Save Database";
            this.mnuSaveDB.Click += new System.EventHandler(this.mnuSaveDB_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(149, 6);
            // 
            // mnuExit
            // 
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Size = new System.Drawing.Size(152, 22);
            this.mnuExit.Text = "E&xit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // saveDbDialog
            // 
            this.saveDbDialog.Filter = "Database|*.db";
            this.saveDbDialog.Title = "Save Database";
            // 
            // openDbDialog
            // 
            this.openDbDialog.FileName = "*.db";
            this.openDbDialog.Filter = "Database|*.db|All Files|*.*";
            this.openDbDialog.Title = "Open Database";
            // 
            // lstUsers
            // 
            this.lstUsers.FormattingEnabled = true;
            this.lstUsers.Location = new System.Drawing.Point(15, 49);
            this.lstUsers.Name = "lstUsers";
            this.lstUsers.Size = new System.Drawing.Size(179, 394);
            this.lstUsers.TabIndex = 9;
            this.lstUsers.SelectedIndexChanged += new System.EventHandler(this.lstUsers_SelectedIndexChanged);
            // 
            // txtEmployeeID
            // 
            this.txtEmployeeID.Location = new System.Drawing.Point(9, 32);
            this.txtEmployeeID.Name = "txtEmployeeID";
            this.txtEmployeeID.Size = new System.Drawing.Size(153, 20);
            this.txtEmployeeID.TabIndex = 10;
            this.txtEmployeeID.Leave += new System.EventHandler(this.txtEmployeeID_Leave);
            // 
            // lblEmployeeID
            // 
            this.lblEmployeeID.AutoSize = true;
            this.lblEmployeeID.Location = new System.Drawing.Point(6, 16);
            this.lblEmployeeID.Name = "lblEmployeeID";
            this.lblEmployeeID.Size = new System.Drawing.Size(67, 13);
            this.lblEmployeeID.TabIndex = 11;
            this.lblEmployeeID.Text = "Employee ID";
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Location = new System.Drawing.Point(185, 16);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(55, 13);
            this.lblUsername.TabIndex = 13;
            this.lblUsername.Text = "Username";
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(188, 32);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(174, 20);
            this.txtUsername.TabIndex = 12;
            this.txtUsername.Leave += new System.EventHandler(this.txtUsername_Leave);
            // 
            // lstDepartmentAccess
            // 
            this.lstDepartmentAccess.FormattingEnabled = true;
            this.lstDepartmentAccess.Location = new System.Drawing.Point(8, 19);
            this.lstDepartmentAccess.Name = "lstDepartmentAccess";
            this.lstDepartmentAccess.Size = new System.Drawing.Size(153, 264);
            this.lstDepartmentAccess.TabIndex = 14;
            this.lstDepartmentAccess.SelectedIndexChanged += new System.EventHandler(this.lstDepartmentAccess_SelectedIndexChanged);
            // 
            // grpAccessLevel
            // 
            this.grpAccessLevel.Controls.Add(this.optCreate);
            this.grpAccessLevel.Controls.Add(this.optDelete);
            this.grpAccessLevel.Controls.Add(this.optWrite);
            this.grpAccessLevel.Controls.Add(this.optRead);
            this.grpAccessLevel.Location = new System.Drawing.Point(167, 72);
            this.grpAccessLevel.Name = "grpAccessLevel";
            this.grpAccessLevel.Size = new System.Drawing.Size(153, 99);
            this.grpAccessLevel.TabIndex = 15;
            this.grpAccessLevel.TabStop = false;
            this.grpAccessLevel.Text = "Access Level";
            // 
            // optCreate
            // 
            this.optCreate.AutoSize = true;
            this.optCreate.Location = new System.Drawing.Point(15, 65);
            this.optCreate.Name = "optCreate";
            this.optCreate.Size = new System.Drawing.Size(57, 17);
            this.optCreate.TabIndex = 3;
            this.optCreate.Text = "Create";
            this.optCreate.UseVisualStyleBackColor = true;
            this.optCreate.CheckedChanged += new System.EventHandler(this.optCreate_CheckedChanged);
            // 
            // optDelete
            // 
            this.optDelete.AutoSize = true;
            this.optDelete.Location = new System.Drawing.Point(87, 65);
            this.optDelete.Name = "optDelete";
            this.optDelete.Size = new System.Drawing.Size(57, 17);
            this.optDelete.TabIndex = 2;
            this.optDelete.Text = "Delete";
            this.optDelete.UseVisualStyleBackColor = true;
            this.optDelete.CheckedChanged += new System.EventHandler(this.optDelete_CheckedChanged);
            // 
            // optWrite
            // 
            this.optWrite.AutoSize = true;
            this.optWrite.Location = new System.Drawing.Point(87, 23);
            this.optWrite.Name = "optWrite";
            this.optWrite.Size = new System.Drawing.Size(51, 17);
            this.optWrite.TabIndex = 1;
            this.optWrite.Text = "Write";
            this.optWrite.UseVisualStyleBackColor = true;
            this.optWrite.CheckedChanged += new System.EventHandler(this.optWrite_CheckedChanged);
            // 
            // optRead
            // 
            this.optRead.AutoSize = true;
            this.optRead.Location = new System.Drawing.Point(15, 23);
            this.optRead.Name = "optRead";
            this.optRead.Size = new System.Drawing.Size(52, 17);
            this.optRead.TabIndex = 0;
            this.optRead.Text = "Read";
            this.optRead.UseVisualStyleBackColor = true;
            this.optRead.CheckedChanged += new System.EventHandler(this.optRead_CheckedChanged);
            // 
            // lblUsers
            // 
            this.lblUsers.AutoSize = true;
            this.lblUsers.Location = new System.Drawing.Point(12, 33);
            this.lblUsers.Name = "lblUsers";
            this.lblUsers.Size = new System.Drawing.Size(34, 13);
            this.lblUsers.TabIndex = 19;
            this.lblUsers.Text = "Users";
            // 
            // grpUserInfo
            // 
            this.grpUserInfo.Controls.Add(this.btnChangePassword);
            this.grpUserInfo.Controls.Add(this.lblEmployeeID);
            this.grpUserInfo.Controls.Add(this.txtEmployeeID);
            this.grpUserInfo.Controls.Add(this.txtUsername);
            this.grpUserInfo.Controls.Add(this.lblUsername);
            this.grpUserInfo.Location = new System.Drawing.Point(200, 49);
            this.grpUserInfo.Name = "grpUserInfo";
            this.grpUserInfo.Size = new System.Drawing.Size(518, 62);
            this.grpUserInfo.TabIndex = 20;
            this.grpUserInfo.TabStop = false;
            this.grpUserInfo.Text = "User Information";
            // 
            // btnChangePassword
            // 
            this.btnChangePassword.Location = new System.Drawing.Point(391, 23);
            this.btnChangePassword.Name = "btnChangePassword";
            this.btnChangePassword.Size = new System.Drawing.Size(112, 23);
            this.btnChangePassword.TabIndex = 20;
            this.btnChangePassword.Text = "Change Password";
            this.btnChangePassword.UseVisualStyleBackColor = true;
            this.btnChangePassword.Click += new System.EventHandler(this.btnChangePassword_Click);
            // 
            // btnSaveChanges
            // 
            this.btnSaveChanges.Location = new System.Drawing.Point(625, 446);
            this.btnSaveChanges.Name = "btnSaveChanges";
            this.btnSaveChanges.Size = new System.Drawing.Size(93, 23);
            this.btnSaveChanges.TabIndex = 21;
            this.btnSaveChanges.Text = "Save Changes";
            this.btnSaveChanges.UseVisualStyleBackColor = true;
            this.btnSaveChanges.Click += new System.EventHandler(this.btnSaveChanges_Click);
            // 
            // cboDepartments
            // 
            this.cboDepartments.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDepartments.FormattingEnabled = true;
            this.cboDepartments.Location = new System.Drawing.Point(173, 34);
            this.cboDepartments.Name = "cboDepartments";
            this.cboDepartments.Size = new System.Drawing.Size(188, 21);
            this.cboDepartments.TabIndex = 22;
            // 
            // lblDepartments
            // 
            this.lblDepartments.AutoSize = true;
            this.lblDepartments.Location = new System.Drawing.Point(170, 18);
            this.lblDepartments.Name = "lblDepartments";
            this.lblDepartments.Size = new System.Drawing.Size(67, 13);
            this.lblDepartments.TabIndex = 23;
            this.lblDepartments.Text = "Departments";
            // 
            // btnAddDepartment
            // 
            this.btnAddDepartment.Location = new System.Drawing.Point(368, 33);
            this.btnAddDepartment.Name = "btnAddDepartment";
            this.btnAddDepartment.Size = new System.Drawing.Size(108, 23);
            this.btnAddDepartment.TabIndex = 24;
            this.btnAddDepartment.Text = "Add Department";
            this.btnAddDepartment.UseVisualStyleBackColor = true;
            this.btnAddDepartment.Click += new System.EventHandler(this.btnAddDepartment_Click);
            // 
            // btnRemoveDepartment
            // 
            this.btnRemoveDepartment.Location = new System.Drawing.Point(8, 289);
            this.btnRemoveDepartment.Name = "btnRemoveDepartment";
            this.btnRemoveDepartment.Size = new System.Drawing.Size(153, 23);
            this.btnRemoveDepartment.TabIndex = 25;
            this.btnRemoveDepartment.Text = "Remove Department";
            this.btnRemoveDepartment.UseVisualStyleBackColor = true;
            this.btnRemoveDepartment.Click += new System.EventHandler(this.btnRemoveDepartment_Click);
            // 
            // grpDepartmentAccess
            // 
            this.grpDepartmentAccess.Controls.Add(this.btnAddDepartment);
            this.grpDepartmentAccess.Controls.Add(this.btnRemoveDepartment);
            this.grpDepartmentAccess.Controls.Add(this.lblDepartments);
            this.grpDepartmentAccess.Controls.Add(this.cboDepartments);
            this.grpDepartmentAccess.Controls.Add(this.lstDepartmentAccess);
            this.grpDepartmentAccess.Controls.Add(this.grpAccessLevel);
            this.grpDepartmentAccess.Location = new System.Drawing.Point(201, 117);
            this.grpDepartmentAccess.Name = "grpDepartmentAccess";
            this.grpDepartmentAccess.Size = new System.Drawing.Size(517, 323);
            this.grpDepartmentAccess.TabIndex = 21;
            this.grpDepartmentAccess.TabStop = false;
            this.grpDepartmentAccess.Text = "Department Access";
            // 
            // btnNewUser
            // 
            this.btnNewUser.Location = new System.Drawing.Point(15, 445);
            this.btnNewUser.Name = "btnNewUser";
            this.btnNewUser.Size = new System.Drawing.Size(75, 23);
            this.btnNewUser.TabIndex = 22;
            this.btnNewUser.Text = "&New User";
            this.btnNewUser.UseVisualStyleBackColor = true;
            this.btnNewUser.Click += new System.EventHandler(this.btnNewUser_Click);
            // 
            // btnDeleteUser
            // 
            this.btnDeleteUser.Location = new System.Drawing.Point(119, 445);
            this.btnDeleteUser.Name = "btnDeleteUser";
            this.btnDeleteUser.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteUser.TabIndex = 23;
            this.btnDeleteUser.Text = "Delete User";
            this.btnDeleteUser.UseVisualStyleBackColor = true;
            this.btnDeleteUser.Click += new System.EventHandler(this.btnDeleteUser_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(733, 480);
            this.Controls.Add(this.btnDeleteUser);
            this.Controls.Add(this.btnNewUser);
            this.Controls.Add(this.grpDepartmentAccess);
            this.Controls.Add(this.btnSaveChanges);
            this.Controls.Add(this.grpUserInfo);
            this.Controls.Add(this.lblUsers);
            this.Controls.Add(this.lstUsers);
            this.Controls.Add(this.mnuMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.mnuMain;
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.Text = "Orinoco Admin";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.mnuMain.ResumeLayout(false);
            this.mnuMain.PerformLayout();
            this.grpAccessLevel.ResumeLayout(false);
            this.grpAccessLevel.PerformLayout();
            this.grpUserInfo.ResumeLayout(false);
            this.grpUserInfo.PerformLayout();
            this.grpDepartmentAccess.ResumeLayout(false);
            this.grpDepartmentAccess.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip mnuMain;
        private System.Windows.Forms.ToolStripMenuItem mnuFile;
        private System.Windows.Forms.ToolStripMenuItem mnuSaveDB;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem mnuExit;
        private System.Windows.Forms.SaveFileDialog saveDbDialog;
        private System.Windows.Forms.OpenFileDialog openDbDialog;
        private System.Windows.Forms.ListBox lstUsers;
        private System.Windows.Forms.TextBox txtEmployeeID;
        private System.Windows.Forms.Label lblEmployeeID;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.ListBox lstDepartmentAccess;
        private System.Windows.Forms.GroupBox grpAccessLevel;
        private System.Windows.Forms.CheckBox optRead;
        private System.Windows.Forms.CheckBox optCreate;
        private System.Windows.Forms.CheckBox optDelete;
        private System.Windows.Forms.CheckBox optWrite;
        private System.Windows.Forms.Label lblUsers;
        private System.Windows.Forms.GroupBox grpUserInfo;
        private System.Windows.Forms.Button btnSaveChanges;
        private System.Windows.Forms.Button btnChangePassword;
        private System.Windows.Forms.Button btnAddDepartment;
        private System.Windows.Forms.Label lblDepartments;
        private System.Windows.Forms.ComboBox cboDepartments;
        private System.Windows.Forms.Button btnRemoveDepartment;
        private System.Windows.Forms.GroupBox grpDepartmentAccess;
        private System.Windows.Forms.ToolStripMenuItem mnuRunBackup;
        private System.Windows.Forms.Button btnNewUser;
        private System.Windows.Forms.Button btnDeleteUser;
    }
}

