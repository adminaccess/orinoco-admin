﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using OrinocoAccess;

namespace Orinoco_Admin
{
    public partial class FormMain : Form
    {
        const string DB_FILE = "users.db";
        const string DEFAULT_PW = "password";
        const int NONE_SELECTED = -1;

        OrinocoUser mUser = null;
        List<Department> mDepartmentsList = new List<Department>(); // parallel list of dept ids with lstDepartmentAccess

        // True if there are changes in the database object
        // that need to be saved to file.
        bool mPendingChanges = false;

        // True if the program is updating the department options due to loading.
        // False if the user updates the department options.
        bool mDeptProgramUpdate = false;

        // If true, don't ask the user if they want to exit.
        bool mDontAskUser = false;

        public FormMain()
        {
            InitializeComponent();
        }

        #region Non-Event Methods
        /// <summary>
        /// Clear all the controls used to display user information
        /// </summary>
        private void ClearUserControls()
        {
            txtEmployeeID.Text = "";
            txtUsername.Text = "";

            lstDepartmentAccess.Items.Clear();
            // Raise this event, since Items.Clear() does not
            lstDepartmentAccess_SelectedIndexChanged(null, null);

            mDepartmentsList.Clear();
            // Raise this event, since Items.Clear() does not
            lstDepartmentAccess_SelectedIndexChanged(null, null);

            optRead.Checked = false;
            optWrite.Checked = false;
            optCreate.Checked = false;
            optDelete.Checked = false;
        }

        /// <summary>
        /// Determine if there are unsaved changes. 
        /// If so, ask the user if they want to save and continue, do not save and continue, or cancel the operation.
        /// </summary>
        /// <returns>Returns true to continue operation, false to cancel the operation.</returns>
        private bool CheckPendingChanges()
        {
            DialogResult saveChanges;

            if (mPendingChanges == true)
            {
                saveChanges = MessageBox.Show("Would you like to save all changes?", "Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                if (saveChanges == DialogResult.Yes) // yes, save changes, continue operation
                {
                    if (Program.gUserDB != null)
                    {
                        Program.gUserDB.Save();
                        PendingChanges(false);
                    }
                    return true;
                }
                else if (saveChanges == DialogResult.No) // no, do not save changes, continue operation
                    return true;
                else // cancel operation
                    return false;
            }
            else // no changes, continue the operation
                return true;
        }

        /// <summary>
        /// Update the selected user's department access levels
        /// </summary>
        private void UpdateDeptAccess()
        {
            DepartmentAccess deptAccess = new DepartmentAccess();

            // Check that a department is selected and that the user updated the options
            if (lstDepartmentAccess.SelectedIndex > NONE_SELECTED && mDeptProgramUpdate == false)
            {
                deptAccess.department = mDepartmentsList[lstDepartmentAccess.SelectedIndex];
                deptAccess.accessLevel = AccessLevel.None;

                if (optRead.Checked)
                    deptAccess.accessLevel = deptAccess.accessLevel | AccessLevel.Read;

                if (optWrite.Checked)
                    deptAccess.accessLevel = deptAccess.accessLevel | AccessLevel.Write;

                if (optCreate.Checked)
                    deptAccess.accessLevel = deptAccess.accessLevel | AccessLevel.Create;

                if (optDelete.Checked)
                    deptAccess.accessLevel = deptAccess.accessLevel | AccessLevel.Delete;

                Program.gUserDB.Users[lstUsers.SelectedIndex].ModifyDeptAccess(deptAccess);
                PendingChanges(true);
            }
        }

        /// <summary>
        /// Set the pending changes flag and
        /// the state of the Save Changes button.
        /// </summary>
        /// <param name="state">The boolean state to change to.</param>
        void PendingChanges(bool state)
        {
            mPendingChanges = state;
            btnSaveChanges.Enabled = state;
            mnuSaveDB.Enabled = state;
        }

        /// <summary>
        /// Disable controls the user does not have access to.
        /// </summary>
        void UpdateCtrlsByPermission()
        {
            if (mUser.HasDeptAccess(Department.Admin, AccessLevel.Delete) == false)
                btnDeleteUser.Enabled = false;

            if (mUser.HasDeptAccess(Department.Admin, AccessLevel.Create) == false)
                btnNewUser.Enabled = false;

            if (mUser.HasDeptAccess(Department.Admin, AccessLevel.Write) == false)
            {
                grpUserInfo.Enabled = false;
                grpAccessLevel.Enabled = false;
                btnAddDepartment.Enabled = false;
                btnRemoveDepartment.Enabled = false;
                cboDepartments.Enabled = false;
            }
        }
        #endregion

        private void FormMain_Load(object sender, EventArgs e)
        {
            // Disable controls until data is loaded
            grpUserInfo.Enabled = false;
            grpAccessLevel.Enabled = false;
            btnRemoveDepartment.Enabled = false;
            grpDepartmentAccess.Enabled = false;
            btnNewUser.Enabled = false;
            btnDeleteUser.Enabled = false;
            btnSaveChanges.Enabled = false;
            mnuSaveDB.Enabled = false;

            // Add the a list of departments to the departments combo box
            foreach (Department dept in Enum.GetValues(typeof(Department)))
                cboDepartments.Items.Add(dept.ToString());
            cboDepartments.Items[0] = "Select a Department";
            cboDepartments.SelectedIndex = 0;

            // Load the user database
            try
            {
                Program.gUserDB = new OrinocoDB(DB_FILE);
                Program.gUserDB.Load();

                foreach (OrinocoUser user in Program.gUserDB.Users)
                    lstUsers.Items.Add(user.Username);

                btnDeleteUser.Enabled = true;
                btnNewUser.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                mDontAskUser = true;
                Close();
            }

            // Check if the user has access
            mUser = Program.gUserDB.GetAuthorisedUser();
            if (mUser == null)
            {                
                MessageBox.Show("Could not validate user!.\nPlease run this program from the Orinoco GUI.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                mDontAskUser = true;
                Close();
            }

            // Disable controls based on user permissions
            UpdateCtrlsByPermission();
        }

        private void mnuSaveDB_Click(object sender, EventArgs e)
        {
            if (Program.gUserDB != null)
            {
                Program.gUserDB.Save();
                PendingChanges(false);
            }
        }

        private void mnuExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void lstUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = -1;

            // If a user is selected
            if (lstUsers.SelectedIndex != NONE_SELECTED)
            {
                // Load user information
                ClearUserControls();
                txtEmployeeID.Text = Program.gUserDB.Users[lstUsers.SelectedIndex].EmployeeID.ToString();
                txtUsername.Text = Program.gUserDB.Users[lstUsers.SelectedIndex].Username;

                foreach (DepartmentAccess dept in Program.gUserDB.Users[lstUsers.SelectedIndex].Departments)
                {
                    index = lstDepartmentAccess.Items.Add(dept.department.ToString());
                    mDepartmentsList.Insert(index, dept.department);
                }

                // Enable since a user is selected
                grpUserInfo.Enabled = true;
                grpDepartmentAccess.Enabled = true;
            }
            else
            {
                // Disable since no user is selected
                grpUserInfo.Enabled = false;
                grpDepartmentAccess.Enabled = false;
            }

            UpdateCtrlsByPermission();
        }

        private void lstDepartmentAccess_SelectedIndexChanged(object sender, EventArgs e)
        {
            // If a department is selected
            if (lstDepartmentAccess.SelectedIndex != NONE_SELECTED)
            {
                AccessLevel accessLevel = Program.gUserDB.Users[lstUsers.SelectedIndex].GetDeptAccessLevel(lstDepartmentAccess.SelectedIndex);

                // Program is updating the department options
                mDeptProgramUpdate = true;

                // Update department access levels
                if ((accessLevel & AccessLevel.Read) != AccessLevel.None)
                    optRead.Checked = true;
                else
                    optRead.Checked = false;

                if ((accessLevel & AccessLevel.Write) != AccessLevel.None)
                    optWrite.Checked = true;
                else
                    optWrite.Checked = false;

                if ((accessLevel & AccessLevel.Create) != AccessLevel.None)
                    optCreate.Checked = true;
                else
                    optCreate.Checked = false;

                if ((accessLevel & AccessLevel.Delete) != AccessLevel.None)
                    optDelete.Checked = true;
                else
                    optDelete.Checked = false;

                // Program finished with updates
                mDeptProgramUpdate = false;

                // Enable since a department has been selected
                grpAccessLevel.Enabled = true;
                btnRemoveDepartment.Enabled = true;
            }
            else
            {
                // Disable since they need a department selected
                grpAccessLevel.Enabled = false;
                btnRemoveDepartment.Enabled = false;
            }

            UpdateCtrlsByPermission();
        }

        private void btnSaveChanges_Click(object sender, EventArgs e)
        {
            Program.gUserDB.Users[lstUsers.SelectedIndex].EmployeeID = uint.Parse(txtEmployeeID.Text);
            Program.gUserDB.Users[lstUsers.SelectedIndex].Username = txtUsername.Text;
            Program.gUserDB.Save();
            PendingChanges(false);
        }

        private void btnAddDepartment_Click(object sender, EventArgs e)
        {
            // if anything after the first item is selected
            if (cboDepartments.SelectedIndex > 0)
            {
                int index = -1;
                DepartmentAccess newDept = new DepartmentAccess();

                // Add new dept to db
                newDept.department = (Department)cboDepartments.SelectedIndex;
                newDept.accessLevel = AccessLevel.None;
                Program.gUserDB.Users[lstUsers.SelectedIndex].AddDepartment(newDept);
                PendingChanges(true);

                // Add new dept to listbox and list
                index = lstDepartmentAccess.Items.Add(cboDepartments.SelectedItem.ToString());
                mDepartmentsList.Insert(index, (Department)cboDepartments.SelectedIndex);
            }
        }

        private void optRead_CheckedChanged(object sender, EventArgs e)
        {
            UpdateDeptAccess();
        }

        private void optWrite_CheckedChanged(object sender, EventArgs e)
        {
            UpdateDeptAccess();
        }

        private void optCreate_CheckedChanged(object sender, EventArgs e)
        {
            UpdateDeptAccess();
        }

        private void optDelete_CheckedChanged(object sender, EventArgs e)
        {
            UpdateDeptAccess();
        }

        private void btnRemoveDepartment_Click(object sender, EventArgs e)
        {
            // If a department is selected
            if (lstDepartmentAccess.SelectedIndex != NONE_SELECTED)
            {
                if (MessageBox.Show("Are you sure you want to remove the department, " + lstDepartmentAccess.SelectedItem.ToString() + "?"
                    , "Remove Department", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    // Remove from database object
                    Program.gUserDB.Users[lstUsers.SelectedIndex].RemoveDepartment(mDepartmentsList[lstDepartmentAccess.SelectedIndex]);

                    // Remove from listbox and list
                    mDepartmentsList.RemoveAt(lstDepartmentAccess.SelectedIndex);
                    lstDepartmentAccess.Items.RemoveAt(lstDepartmentAccess.SelectedIndex);

                    PendingChanges(true);
                }
            }
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            // If we should ask the user, then continue
            if (mDontAskUser == false)
            {
                // If there were changes and the user didn't cancel, continue
                if (CheckPendingChanges() == true)
                {
                    if (MessageBox.Show("Are you sure you want to exit?", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        e.Cancel = true;
                }
                else
                    e.Cancel = true;
            }
        }

        private void btnChangePassword_Click(object sender, EventArgs e)
        {
            // If a user is selected
            if (lstUsers.SelectedIndex != NONE_SELECTED)
            {
                FormChangePW frmChangePW = new FormChangePW(lstUsers.SelectedIndex);

                if (frmChangePW.ShowDialog() == DialogResult.OK)
                    PendingChanges(true);
            }
        }

        private void txtEmployeeID_Leave(object sender, EventArgs e)
        {
            // If a user is selected
            if (lstUsers.SelectedIndex != NONE_SELECTED)
            {
                // If a change was made in the textbox, update the db
                if (uint.Parse(txtEmployeeID.Text) != Program.gUserDB.Users[lstUsers.SelectedIndex].EmployeeID)
                {
                    Program.gUserDB.Users[lstUsers.SelectedIndex].EmployeeID = uint.Parse(txtEmployeeID.Text);
                    PendingChanges(true);
                }
            }
        }

        private void txtUsername_Leave(object sender, EventArgs e)
        {
            // If a user is selected
            if (lstUsers.SelectedIndex != NONE_SELECTED)
            {
                // If a change was made in the textbox, update the db
                if (txtUsername.Text != Program.gUserDB.Users[lstUsers.SelectedIndex].Username)
                {
                    Program.gUserDB.Users[lstUsers.SelectedIndex].Username = txtUsername.Text;
                    PendingChanges(true);
                    lstUsers.Items[lstUsers.SelectedIndex] = txtUsername.Text;
                }
            }
        }

        private void mnuRunBackup_Click(object sender, EventArgs e)
        {
            Process proc = new Process();
            proc.StartInfo.FileName = "OrinocoBackup.exe";

            try
            {
                proc.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Run Backup", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnNewUser_Click(object sender, EventArgs e)
        {
            int position = -1;
            OrinocoUser newUser = new OrinocoUser();

            newUser.EmployeeID = 0;
            newUser.Username = "New User";
            newUser.Password = Hash.ToHash(DEFAULT_PW);
            Program.gUserDB.Users.Add(newUser);
            PendingChanges(true);

            position = lstUsers.Items.Add(newUser.Username);
            if (position > -1)
            {
                lstUsers.SelectedIndex = position;
                txtEmployeeID.Text = newUser.EmployeeID.ToString();
                txtUsername.Text = newUser.Username;
            }
        }

        private void btnDeleteUser_Click(object sender, EventArgs e)
        {
            // If a user is selected
            if (lstUsers.SelectedIndex != NONE_SELECTED)
            {
                // Check if the selected user is the current user
                if (lstUsers.SelectedItem.ToString() == mUser.Username)
                    MessageBox.Show("You can't delete your own account.", "Delete User", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                else if (MessageBox.Show("Are you sure you want to remove the user, " + txtUsername.Text + "?",
                    "Delete User", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Program.gUserDB.Users.RemoveAt(lstUsers.SelectedIndex);
                    PendingChanges(true);

                    lstUsers.Items.RemoveAt(lstUsers.SelectedIndex);
                    ClearUserControls();
                }
            }
            else
                MessageBox.Show("Please select a user to delete.");
        }
    }
}
