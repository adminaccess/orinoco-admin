﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OrinocoAccess;

namespace Orinoco_Admin
{
    public partial class FormChangePW : Form
    {
        int mUserIndex;

        public FormChangePW(int userIndex)
        {
            InitializeComponent();

            if (userIndex > -1)
                mUserIndex = userIndex;
            else
                this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string caption = "Change Password";

            if (txtNewPassword.Text == "" || txtConfirmPassword.Text == "")
            {
                MessageBox.Show("Please enter a new password and type it in again to confirm.", caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtNewPassword.Focus();
            }
            else if (txtConfirmPassword.Text != txtNewPassword.Text)
            {
                MessageBox.Show("The passwords do not match.", caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtNewPassword.Text = "";
                txtConfirmPassword.Text = "";
                txtNewPassword.Focus();
            }
            else
            {
                Program.gUserDB.Users[mUserIndex].Password = Hash.ToHash(txtNewPassword.Text);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
    }
}
